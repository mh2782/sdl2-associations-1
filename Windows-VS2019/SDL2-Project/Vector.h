#ifndef VECTOR2F_H_
#define VECTOR2F_H_

#include "SDL2Common.h"

class Vector2f {
private:
	float x;
	float y;

public:
	//Constructors
	Vector2f();
	Vector2f(float x, float y);
	Vector2f(Vector2f* other);

	//Destructor
	~Vector2f();

	void normalise();
	float getLength();

	void zero();

	void setX(float x);
	float getX(void);

	void setY(float y);
	float getY(void);

	void add(Vector2f* other);

	void scale(float scalar);
};

#endif
#include "Player.h"
#include "Animation.h"
#include "Vector.h"

#include "TextureUtils.h"

#include <stdexcept>

/**
 * Player
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
Player::Player()
{
    speed = 50.0f;

    //Postion   
    position = nullptr;
    //Velocity
    velocity = nullptr;

    // set the default state to idle 
    state = IDLE;

    // Set texture pointer
    texture = nullptr;

    // Target is the same size of the source
    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;

    // Better initialise our texture pointers to null also
    for (int i = 0; i < MAX_ANIMATIONS; i++)
    {
       animations[i] = nullptr;
    }
}


/**
 * initPlayer
 * 
 * Function to populate an animation structure from given paramters. 
 * 
 * @param player Player structure to populate 
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void Player::init(SDL_Renderer *renderer)
{
    //Allocate position and velocity - set values
    position = new Vector2f(200.0f, 200.0f);
    velocity = new Vector2f();
    velocity->zero();

    // Create player texture from file, optimised for renderer 

    texture = createTextureFromFile("assets/images/undeadking.png", renderer);

    if(texture == nullptr)
        throw std::runtime_error("File not found!"); 

    // Allocate memory for the animation structures
    for (int i = 0; i < MAX_ANIMATIONS; i++)
    {
       animations[i] = new Animation();
    }
    
    // Setup the animation structure
    animations[LEFT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[RIGHT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[UP]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[DOWN]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);

}

/**
 * ~Player
 * 
 * Destroys the player and any associated 
 * objects 
 * 
 */
Player::~Player()
{
    //De-allocate memory. Set pointers to null
    delete position;
    position = nullptr;

    delete velocity;
    velocity = nullptr;

    // Clean up animations - free memory
    for (int i = 0; i < MAX_ANIMATIONS; i++)
    {
        // Clean up the animaton structure
        // allocated with new so use delete. 
        delete animations[i];
        animations[i] = nullptr;
    }

    // Clean up 
    SDL_DestroyTexture(texture);
    texture = nullptr; 
}

/**
 * draw
 * 
 * Method draw a Player object
 * 
 * @param renderer SDL_Renderer to draw to
 */
void Player::draw(SDL_Renderer *renderer)
{
    // Get current animation based on the state. 
    Animation* current = this->animations[state];

    SDL_RenderCopy(renderer, texture, current->getCurrentFrame(), &targetRectangle);
}

/**
 * processInput
 * 
 * Method to process inputs for the player 
 * Note: Need to think about other forms of input!
 * 
 * @param keyStates The keystates array. 
 */
void Player::processInput(const Uint8 *keyStates)
{
    // Process Player Input

    //Input - keys/joysticks?
    float verticalInput = 0.0f; 
    float horizontalInput = 0.0f; 

    // If no keys are down player should not animate!
    state = IDLE;

    // This could be more complex, e.g. increasing the vertical 
    // input while the key is held down. 
    if (keyStates[SDL_SCANCODE_UP]) 
    {
        verticalInput = -1.0f;
        state = UP;
    }

    if (keyStates[SDL_SCANCODE_DOWN]) 
    {
        verticalInput = 1.0f;
        state = DOWN;
    }
  
    if (keyStates[SDL_SCANCODE_RIGHT]) 
    {
        horizontalInput = 1.0f;
        state = RIGHT;
    }

    if (keyStates[SDL_SCANCODE_LEFT]) 
    {
        horizontalInput = -1.0f;
        state = LEFT;
    }

    // Calculate player velocity. 
    // Note: This is imperfect, no account taken of diagonal!
    velocity->setX(horizontalInput);
    velocity->setY(verticalInput);
    velocity->normalise();
    velocity->scale(speed);
}

/**
 * update
 * 
 * Method to update the player 
 * 
 * @param timeDeltaInSeconds the time delta in seconds
 */
void Player::update(float timeDeltaInSeconds)
{
    // Calculate distance travelled since last update
    Vector2f movement(velocity);
    movement.scale(timeDeltaInSeconds);

    // Update player position.
    position->add(&movement);

    // Move sprite to nearest pixel location.
    targetRectangle.y = round(position->getY());
    targetRectangle.x = round(position->getX());

    // Get current animation
    Animation* current = animations[state];

    // let animation update itself. 
    current->update(timeDeltaInSeconds);
}